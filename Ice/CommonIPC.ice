﻿// Copyright 2016 Complete Genomics, Inc.  All Rights Reserved.
// Confidential and proprietary works of Complete Genomics, Inc.

module BGI {
	module RPC {
        exception BGIRemoteException
        {
            string message;
        };
	};
};
