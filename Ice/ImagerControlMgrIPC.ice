﻿// Copyright 2016 Complete Genomics, Inc.  All Rights Reserved.
// Confidential and proprietary works of Complete Genomics, Inc.

#include "Ice/Identity.ice"
#include "./StageRunMgrIPC.ice"

module BGI {
	module RPC {
	
		struct ImagerRunInfo
		{
			string Barcode;
			int CurrentRow;
			int CurrentCol;
			int CurrentCycle;
			int CurrentLane;
			int TotalRow;
			int TotalCol;
			int TotalLane;
			int ScanDirect;
		};

	   ////DipSeq-->Imager error to clients
		enum ImagerErrorEnum 
		{
		    Findlocation,
			ThetaCorrection,
			StageError,
			CameraError,
			LaserError,
			IOError,
			XLP6KPumpError,
			BasecallingError,
			ImagerStop,//Stop imager,cycle not change
			SeqDataValid,// Sequencing data error
			ConfigDataError,
			ScriptError,
			ChipPositionError,
			ScanRowColError,
			None
		};

		sequence<double> ExposureTimeArray;
		struct ImagerConfigData
		{
			int Lane;
			string Barcode;
			string Reference;

			bool BasecallMode;
			int GapRow;
			int GapCol;
			bool IsSaveRawImage;
			bool IsImageSimulated;
			int SettingTime;

			int CheckThetaMode;
			int Direction;
			int StartRow;
			int EndRow;
			int StartCol;
			int EndCol;

			bool IsSaveRawInt;
			bool IsSaveMidInt;
			bool IsSaveFinInt;
			bool IsSaveCallFiles;

			ExposureTimeArray ExposureTime;
		};

		dictionary<string, string> SpeciesBarcodesDatas;
		struct ImagerSetupInfo
		{
			string SlideID;
			string ScriptName;			
			string ExperimentName;
			string UserName;
			int ImagerID;

			int ZAFOffset;
			bool IsCalAFOffset;
			//int ReactionNum;
			int Read1Len;
			int Read2Len;
			int BarcodeLen;

			string Reference;
			int ScanLane;
			SpeciesBarcodesDatas BioBarcode;

			int TotalCycle;
			int BiochemCycle;
			int ImagerCycle;

			int TolerancePattern;

			int StartRow;
			int EndRow;
			int StartCol;
			int EndCol;
		};
								
		interface ImagerControlMgrIPC extends StageRunMgrIPC
		{
			bool ImagerSetup(ImagerSetupInfo info);
			
			bool GoHome();
			bool GoLoadingPos();
			bool GoWashPos();
			bool IsAtLoadPosition();
			bool SwitchVaccum(bool status);
			bool GetVaccumStatus();
			bool PreInjectSRE();
			
			bool ImagerConfigSet(ImagerConfigData config);
			ImagerConfigData ImagerConfigGet();			
		};

		interface ImagerControlMgrIPCCallback extends StageRunMgrIPCCallback
		{
			void SendImagerError(ImagerErrorEnum error);
			void SendImagerRunInfo(ImagerRunInfo info);
			void SendImagerTaskEnd(ImagerSetupInfo info, ImagerErrorEnum error);
		};
	};
};
