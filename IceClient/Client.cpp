#include "Client.h"
#include <Ice\Connection.h>
#include <IceUtil\IceUtil.h>
#include "../cpplib/cpplogger.h"
#include "ImagerControlMgrCallbackClientImp.h"

using namespace std;

class ClientConnMonitor : public Ice::ConnectionCallback
{
public:
	ClientConnMonitor(IceClient* client) : m_client(client)
	{

	}

	void heartbeat(const ::Ice::ConnectionPtr& conn)
	{
		//CPP_LOG_INFO("Get Server Hearbeat, times = %d", ++m_count);
	}

	void closed(const ::Ice::ConnectionPtr& conn)
	{
		m_client->SetHostLost();
		CPP_LOG_INFO("Server Close");
	}

private:
	IceClient* m_client = nullptr;
	int m_count = 0;
};

IceClient::IceClient()
{
	CPP::CppLogger::Initialize(CPP::CppLogger::LOG_ALL_OUT, 31, ".\\logs\\client");
	CPP::CppLogger::Instance()->EnableDetail(false);

	//创建Ice环境
	m_ic = Ice::initialize();

	//加载配置文件
	auto props = m_ic->getProperties();
	props->load("settings.config");

	//创建本地回调接口对象
	m_LocalAdapter = m_ic->createObjectAdapter("");
	m_LocalIdent.name = IceUtil::generateUUID();
	m_LocalIdent.category = "";
	Ice::ObjectPtr object = new ImagerControlMgrCallbackClientImp;
	m_LocalAdapter->add(object, m_LocalIdent);
	m_LocalAdapter->activate();

	CPP_LOG_INFO("Client Start");
}

IceClient::~IceClient()
{
	if (m_ic)
	{
		m_ic->shutdown();
		m_ic->waitForShutdown();
		m_ic->destroy();
		CPP_LOG_INFO("Client Close");
	}
}

bool IceClient::IsHostActive()
{
	// 连接或重连服务器
	if (!m_bHostActive)
	{
		Connect();
	}
	return m_bHostActive;
}

void IceClient::Connect()
{
	try
	{
		CPP_LOG_INFO("Connnet to server...");

		m_ImagerControlMgr = nullptr;
		m_ImagerControlMgrCB = nullptr;

		//连接到服务器获取远程对象
		auto props = m_ic->getProperties();
		Ice::ObjectPrx base = m_ic->stringToProxy(props->getProperty("Identity") + string(":") + props->getProperty("clientEndpoint"));
		Ice::ObjectPrx baseCB = m_ic->stringToProxy(props->getProperty("IdentityCB") + string(":") + props->getProperty("clientEndpointCB"));
		m_ImagerControlMgr = BGI::RPC::ImagerControlMgrIPCPrx::checkedCast(base);
		m_ImagerControlMgrCB = BGI::RPC::ImagerControlMgrIPCCallbackPrx::checkedCast(baseCB);

		//增加ICE连接对象的回调接口
		m_ImagerControlMgrCB->ice_getConnection()->setCallback(new ClientConnMonitor(this));

		//获得连接对象并将适配器传递给连接对象
		m_ImagerControlMgrCB->ice_getConnection()->setAdapter(m_LocalAdapter);

		// 4.传递本地服务对象给服务器用于回调
		CPP_LOG_INFO("Add client to server");
		m_ImagerControlMgrCB->AddClient(m_LocalIdent);

		CPP_LOG_INFO("Connnet success");

		m_bHostActive = true;
	}
	catch (const Ice::Exception& e) {
		CPP_LOG_ERROR(e.what());
	}
	catch (const std::string& msg) {
		CPP_LOG_ERROR(msg.c_str());
	}
	catch (const char* msg) {
		CPP_LOG_ERROR(msg);
	}
	catch (...) {
		CPP_LOG_ERROR("Unknown error");
	}
}

bool IceClient::ImagerSetup(const::BGI::RPC::ImagerSetupInfo & info)
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->ImagerSetup(info);
	}
	return false;
}

bool IceClient::GoHome()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->GoHome();
	}
	return false;
}

bool IceClient::GoLoadingPos()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->GoLoadingPos();
	}
	return false;
}

bool IceClient::GoWashPos()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->GoWashPos();
	}
	return false;
}

bool IceClient::IsAtLoadPosition()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->IsAtLoadPosition();
	}
	return false;
}

bool IceClient::SwitchVaccum(bool value)
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->SwitchVaccum(value);
	}
	return false;
}

bool IceClient::GetVaccumStatus()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->GetVaccumStatus();
	}
	return false;
}

bool IceClient::PreInjectSRE()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->PreInjectSRE();
	}
	return false;
}

bool IceClient::ImagerConfigSet(const::BGI::RPC::ImagerConfigData & config)
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->ImagerConfigSet(config);
	}
	return false;
}

::BGI::RPC::ImagerConfigData IceClient::ImagerConfigGet()
{
	if (IsHostActive()) {
		return m_ImagerControlMgr->ImagerConfigGet();
	}
	return ::BGI::RPC::ImagerConfigData();
}
