#pragma once

#include <Ice/Ice.h>
#include "ImagerControlMgrIPC.h"

class IceClient
{
public:
	IceClient();
	~IceClient();

	void SetHostLost() { m_bHostActive = false; }
	bool IsHostActive();
	void Connect();

	// Implment functions
	bool ImagerSetup(const::BGI::RPC::ImagerSetupInfo & info);
	bool GoHome();
	bool GoLoadingPos();
	bool GoWashPos();
	bool IsAtLoadPosition();
	bool SwitchVaccum(bool value);
	bool GetVaccumStatus();
	bool PreInjectSRE();
	bool ImagerConfigSet(const::BGI::RPC::ImagerConfigData & config);
	::BGI::RPC::ImagerConfigData ImagerConfigGet();

private:
	bool m_bHostActive = false;
	Ice::Identity m_LocalIdent;
	Ice::CommunicatorPtr m_ic = nullptr;
	Ice::ObjectAdapterPtr m_LocalAdapter = nullptr;
	BGI::RPC::ImagerControlMgrIPCPrx m_ImagerControlMgr = nullptr;
	BGI::RPC::ImagerControlMgrIPCCallbackPrx m_ImagerControlMgrCB = nullptr;
};
