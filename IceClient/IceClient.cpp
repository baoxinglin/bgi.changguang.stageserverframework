#include "Client.h"
#include <iostream>
#include "../cpplib/cpplogger.h"

using namespace std;
using namespace BGI::RPC;

int main(int argc, char* argv[])
{
	ImagerSetupInfo setupInfo;

	int cmd = -1;
	int val = -1;
	try
	{
		IceClient client;

		cout << "0 exit" << endl
			<< "1 ImagerSetup" << endl
			<< "2 GoHome" << endl
			<< "3 GoLoadingPos" << endl
			<< "4 GoWashPos" << endl
			<< "5 IsAtLoadPosition" << endl
			<< "6 SwitchVaccum" << endl
			<< "7 GetVaccumStatus" << endl
			<< "8 PreInjectSRE" << endl
			<< "9 ImagerConfigSet" << endl
			<< "10 ImagerConfigGet" << endl;

		while (true)
		{
			cin >> cmd;
			switch (cmd)
			{
			case 0:
				cout << "Press any key to exit" << endl;
				cin >> cmd;
				return 0;
			case 1:
				client.ImagerSetup(setupInfo);
				break;
			case 2:
				client.GoHome();
				break;
			case 3:
				client.GoLoadingPos();
				break;
			case 4:
				client.GoWashPos();
				break;
			case 5:
				cout << "IsAtLoadPosition : " << (client.IsAtLoadPosition() ? "True" : "False") << endl;
				break;
			case 6:
				cout << "Input VaccumStatus : " << endl;
				cin >> val;
				client.SwitchVaccum((val != 0) ? true : false);
				break;
			case 7:
				cout << "GetVaccumStatus : " << (client.GetVaccumStatus() ? "True" : "False") << endl;
				break;
			case 8:
				client.PreInjectSRE();
				break;
			case 9:
				break;
			case 10:
				break;
			}
		}
	}
	catch (const Ice::Exception& e) {
		CPP_LOG_ERROR(e.what());
	}
	catch (const std::string& msg) {
		CPP_LOG_ERROR(msg.c_str());
	}
	catch (const char* msg) {
		CPP_LOG_ERROR(msg);
	}
	catch (...) {
		CPP_LOG_ERROR("Unknown error");
	}

	cin >> cmd;

	return 0;
}
