#pragma once

#include <mutex>
#include <atomic>
#include <thread>
#include <Ice/Ice.h>
#include <Ice\Connection.h>
#include "cpplogger.h"

class IceClient
{
protected:
	class ConnCallBackProc : public Ice::ConnectionCallback
	{
	public:
		ConnCallBackProc(IceClient* client) : _ptrClient(client)
		{

		}

		void heartbeat(const ::Ice::ConnectionPtr& conn)
		{
			//CPP_LOG_INFO("BasecallClient Hearbeat, times = %d", _nHeartbeatCount++);
		}

		void closed(const ::Ice::ConnectionPtr& conn)
		{
			_ptrClient->SetHostUnAlive();
			CPP_LOG_INFO("Basecall Client Close");
		}

	private:
		IceClient* _ptrClient = nullptr;
		unsigned int _nHeartbeatCount = 0;
	};

public:
	bool Initialize();
	bool IsHostAlive(int timeout_ms = -1);
	inline void StopReconn() { _bStopReconn = true; }
	inline void SetHostUnAlive() { _bHostAlive = false; }

protected:
	IceClient(bool bMonitorReconn, bool bFuncInReconn, int nReconnFreq = 100, int nMaxReconnCount = 900, int nHeartbeatFreq = 1000);
	virtual ~IceClient();

	inline void EnableStopReconn() { _bStopReconn = false; }
	inline bool CheckStopReconn() { return _bStopReconn; }

	bool CheckHostAlive();
	bool Connect(bool enableStopReconn);

	virtual void CreateLocalAdpter() = 0;
	virtual void CreateRemoteProxy() = 0;

protected:
	const int _nReconnFreq = 100;
	const int _nMaxReconnCount = 900;
	const int _nHeartbeatFreq = 1000;
	const bool _bFuncInReconn = true;
	const bool _bMonitorReconn = true;
	std::atomic_bool _bClientExit = ATOMIC_VAR_INIT(false);
	std::atomic_bool _bStopReconn = ATOMIC_VAR_INIT(false);
	std::atomic_bool _bHostAlive = ATOMIC_VAR_INIT(false);
	Ice::CommunicatorPtr _ic;
	std::thread _thread;
	std::mutex _mtConn;
};
