#include "ImagerControlMgrCallbackClientImp.h"

using namespace std;

ImagerControlMgrCallbackClientImp::ImagerControlMgrCallbackClientImp()
{
}

ImagerControlMgrCallbackClientImp::~ImagerControlMgrCallbackClientImp()
{
}

void ImagerControlMgrCallbackClientImp::AddClient(const::Ice::Identity &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendScriptName(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendActionName(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendFlowcellBarcode(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendPositionNumber(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendPcntDone(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendScriptStatus(::BGI::RPC::ScriptStateEnum, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendScriptMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendStageMessage(const::std::string &, const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendQCValue(const::BGI::RPC::QCDataValue &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendQCValues(const::BGI::RPC::QCDataValues &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendAlarmMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendLogMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendRemainingTime(::Ice::Double, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendDataList(const::BGI::RPC::DataList &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackClientImp::SendImagerError(::BGI::RPC::ImagerErrorEnum err, const::Ice::Current &cur)
{
	const char* szImagerError[] = 
	{
		"Findlocation",
		"ThetaCorrection",
		"StageError",
		"CameraError",
		"LaserError",
		"IOError",
		"XLP6KPumpError",
		"BasecallingError",
		"ImagerStop",
		"SeqDataValid",
		"ConfigDataError",
		"ScriptError",
		"ChipPositionError",
		"ScanRowColError",
		"None"
	};
	cout << "Receive Imager Error : " << szImagerError[err] << endl;
}

void ImagerControlMgrCallbackClientImp::SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo &info, const::Ice::Current &cur)
{
	cout << "Receive Run Info =>" << endl
		<< "	Barcode : " << info.Barcode.c_str() << endl
		<< "	CurrentLane : " << info.CurrentLane << endl
		<< "	CurrentCycle : " << info.CurrentCycle << endl
		<< "	CurrentCol : " << info.CurrentCol << endl
		<< "	CurrentRow : " << info.CurrentRow << endl
		<< "	TotalCol : " << info.TotalCol << endl
		<< "	TotalRow : " << info.TotalRow << endl
		<< "	TotalLane : " << info.TotalLane << endl;
}

void ImagerControlMgrCallbackClientImp::SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo &info, ::BGI::RPC::ImagerErrorEnum, const::Ice::Current &cur)
{
	cout << "Receice TaskEnd Info =>" << endl
		<< "	SlideID : " << info.SlideID.c_str() << endl
		<< "	ScanLane : " << info.ScanLane << endl
		<< "	ImagerCycle : " << info.ImagerCycle << endl
		<< "	TotalCycle : " << info.TotalCycle << endl
		<< "	StartCol : " << info.StartCol << endl
		<< "	EndCol : " << info.EndCol << endl
		<< "	StartRow : " << info.StartRow << endl
		<< "	EndRow : " << info.EndRow << endl
		<< "	Read1Len : " << info.Read1Len << endl
		<< "	Read2Len : " << info.Read2Len << endl
		<< "	BarcodeLen : " << info.BarcodeLen << endl;
}
