#pragma once

#include "ImagerControlMgrIPC.h"

class ImagerControlMgrCallbackClientImp : public BGI::RPC::ImagerControlMgrIPCCallback
{
public:
	ImagerControlMgrCallbackClientImp();
	~ImagerControlMgrCallbackClientImp();

	// Inherited via ImagerControlMgrIPCCallback
	virtual void AddClient(const::Ice::Identity &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendScriptName(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendActionName(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendFlowcellBarcode(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendPositionNumber(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendPcntDone(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendScriptStatus(::BGI::RPC::ScriptStateEnum, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendScriptMessageId(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendStageMessage(const::std::string &, const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendQCValue(const::BGI::RPC::QCDataValue &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendQCValues(const::BGI::RPC::QCDataValues &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendAlarmMessageId(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendLogMessageId(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendRemainingTime(::Ice::Double, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendDataList(const::BGI::RPC::DataList &, const::Ice::Current & = ::Ice::Current()) override;
	
	// Client Implement
	virtual void SendImagerError(::BGI::RPC::ImagerErrorEnum, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo &, ::BGI::RPC::ImagerErrorEnum, const::Ice::Current & = ::Ice::Current()) override;
};

