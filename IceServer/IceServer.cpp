#include "Server.h"
#include "cpplogger.h"
#include "StageInterface.h"

using namespace std;

class SimuStage : public StageControl
{
public:
	// Inherited via StageControl
	virtual bool ImagerSetup(const::BGI::RPC::ImagerSetupInfo & info) override
	{
		m_cb->SendImagerError(BGI::RPC::ImagerErrorEnum::BasecallingError);
		cout << "ImagerSetup" << endl;
		return true;
	}
	virtual bool GoHome() override
	{
		cout << "GoHome" << endl;
		return true;
	}
	virtual bool GoLoadingPos() override
	{
		cout << "GoLoadingPos" << endl;
		return true;
	}
	virtual bool GoWashPos() override
	{
		cout << "GoWashPos" << endl;
		return true;
	}
	virtual bool IsAtLoadPosition() override
	{
		cout << "IsAtLoadPosition" << endl;
		return true;
	}
	virtual bool SwitchVaccum(bool value) override
	{
		cout << "SwitchVaccum" << endl;
		return true;
	}
	virtual bool GetVaccumStatus() override
	{
		cout << "GetVaccumStatus" << endl;
		return true;
	}
	virtual bool PreInjectSRE() override
	{
		cout << "PreInjectSRE" << endl;
		return true;
	}
	virtual bool ImagerConfigSet(const::BGI::RPC::ImagerConfigData & config) override
	{
		cout << "ImagerConfigSet" << endl;
		return true;
	}
	virtual ::BGI::RPC::ImagerConfigData ImagerConfigGet() override
	{
		return ::BGI::RPC::ImagerConfigData();
	}
};

int main(int argc, char* argv[])
{
	try
	{
		SimuStage stage;
		IceServer server(&stage);
		stage.SetCallback(&server);
		server.main(argc, argv);
	}
	catch (const Ice::Exception& e) {
		CPP_LOG_ERROR(e.what());
	}
	catch (const std::string& msg) {
		CPP_LOG_ERROR(msg.c_str());
	}
	catch (const char* msg) {
		CPP_LOG_ERROR(msg);
	}
	catch (...) {
		CPP_LOG_ERROR("Unknown error");
	}

	int cmd;
	std::cin >> cmd;

	return 0;
}
