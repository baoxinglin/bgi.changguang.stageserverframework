#include "ImagerControlMgrCallbackServerImp.h"
#include <Ice/Connection.h>
#include "cpplogger.h"

class ServerConnMonitor : public Ice::ConnectionCallback
{
public:
	ServerConnMonitor(ImagerControlMgrCallbackServerImp* cb) : m_cbImp(cb)
	{

	}

	void heartbeat(const ::Ice::ConnectionPtr& conn)
	{
		//CPP_LOG_INFO("Get Server Hearbeat, times = %d", ++m_count);
	}

	void closed(const ::Ice::ConnectionPtr& conn)
	{
		m_cbImp->SetClientLost();
		CPP_LOG_INFO("Server Close");
	}

private:
	ImagerControlMgrCallbackServerImp* m_cbImp = nullptr;
	int m_count = 0;
};

ImagerControlMgrCallbackServerImp::ImagerControlMgrCallbackServerImp()
{
}

ImagerControlMgrCallbackServerImp::~ImagerControlMgrCallbackServerImp()
{
}

void ImagerControlMgrCallbackServerImp::AddClient(const::Ice::Identity & ident, const::Ice::Current &cur)
{
	try
	{
		std::lock_guard<std::mutex> lock(m_addmt);
		Ice::ObjectPrx raw = cur.con->createProxy(ident);
		m_client = BGI::RPC::ImagerControlMgrIPCCallbackPrx::uncheckedCast(raw);
		if (m_client)
		{
			//增加ICE连接对象的回调接口
			cur.con->setCallback(new ServerConnMonitor(this));
			cur.con->setACM(2, Ice::CloseOff, Ice::ACMHeartbeat::HeartbeatAlways);
			CPP_LOG_INFO("Client Add Success");
		}
		else
		{
			CPP_LOG_INFO("Client Add Fail");
		}
	}
	catch (const std::exception& e)
	{
		CPP_LOG_ERROR("[Exception]%s", e.what());
	}
	catch (...)
	{
		CPP_LOG_ERROR("[Unknown Exception]");
	}
}

void ImagerControlMgrCallbackServerImp::SendScriptName(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendActionName(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendFlowcellBarcode(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendPositionNumber(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendPcntDone(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendScriptStatus(::BGI::RPC::ScriptStateEnum, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendScriptMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendStageMessage(const::std::string &, const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendQCValue(const::BGI::RPC::QCDataValue &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendQCValues(const::BGI::RPC::QCDataValues &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendAlarmMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendLogMessageId(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendRemainingTime(::Ice::Double, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendDataList(const::BGI::RPC::DataList &, const::Ice::Current &cur)
{
}

void ImagerControlMgrCallbackServerImp::SendImagerError(::BGI::RPC::ImagerErrorEnum err, const::Ice::Current &cur)
{
	if (m_client) {
		m_client->SendImagerError(err);
	}
}

void ImagerControlMgrCallbackServerImp::SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo & info, const::Ice::Current &cur)
{
	if (m_client) {
		m_client->SendImagerRunInfo(info);
	}
}

void ImagerControlMgrCallbackServerImp::SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo & info, ::BGI::RPC::ImagerErrorEnum err, const::Ice::Current &cur)
{
	if (m_client) {
		m_client->SendImagerTaskEnd(info, err);
	}
}
