#include "ImagerControlMgrImp.h"

ImagerControlMgrImp::ImagerControlMgrImp(StageControl* stage) : m_stage(stage)
{
}

ImagerControlMgrImp::~ImagerControlMgrImp()
{
}

::Ice::Int ImagerControlMgrImp::GetHeartbeat(const::Ice::Current &cur)
{
	return ::Ice::Int();
}

void ImagerControlMgrImp::StartSetup(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SaveSelectedScriptFileName(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::ScanFlowcellBarcode(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::ScanReagentKitBarcode(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::OverrideFlowcellBarcode(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SendReagentKit(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::StartRun(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::PauseScript(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::ResumeScript(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::StopScript(const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::AbortScript(const::Ice::Current &cur)
{
}

::Ice::Int ImagerControlMgrImp::RunScriptNow(const::std::string &, const::Ice::Current &cur)
{
	return ::Ice::Int();
}

::BGI::RPC::ScriptRunInfo ImagerControlMgrImp::GetScriptRunInfo(::Ice::Int, const::Ice::Current &cur)
{
	return ::BGI::RPC::ScriptRunInfo();
}

::BGI::RPC::MessageArray ImagerControlMgrImp::GetScriptMessages(::Ice::Int, const::Ice::Current &cur)
{
	return ::BGI::RPC::MessageArray();
}

void ImagerControlMgrImp::SetupScriptFile(const::std::string &, const::Ice::Current &cur)
{
}

::BGI::RPC::QCDataValues ImagerControlMgrImp::GetQCValues(const::std::string &, ::Ice::Int, const::Ice::Current &cur)
{
	return ::BGI::RPC::QCDataValues();
}

void ImagerControlMgrImp::SendSampleId(const::std::string &, const::Ice::Current &cur)
{
}

::BGI::RPC::LoginedInfo ImagerControlMgrImp::Authenicate(const::std::string &, const::std::string &, const::Ice::Current &cur)
{
	return ::BGI::RPC::LoginedInfo();
}

::std::string ImagerControlMgrImp::GetLanguage(const::Ice::Current &cur)
{
	return ::std::string();
}

void ImagerControlMgrImp::SetLanguage(const::std::string &, const::Ice::Current &cur)
{
}

::BGI::RPC::ExperimentInfo ImagerControlMgrImp::GetExperimentInfo(const::Ice::Current &cur)
{
	return ::BGI::RPC::ExperimentInfo();
}

void ImagerControlMgrImp::DownloadScriptFile(::std::string &, ::std::string &, ::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::UIOperation(const::std::string &, const::std::string &, const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::StartSelfCheck(::BGI::RPC::DeviceCheckTypeEnum, const::Ice::Current &cur)
{
}

::BGI::RPC::AlarmMessageList ImagerControlMgrImp::GetAlarmMessages(::Ice::Int, const::Ice::Current &cur)
{
	return ::BGI::RPC::AlarmMessageList();
}

::BGI::RPC::LogMessageList ImagerControlMgrImp::GetLogMessages(::Ice::Int, const::Ice::Current &cur)
{
	return ::BGI::RPC::LogMessageList();
}

void ImagerControlMgrImp::ClearAlarmMessages(const::BGI::RPC::ClearAlarmMessageIds &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::ReagentNeedle(bool, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SendExperimentType(const::std::string &, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SetBuzzerVolume(::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SetBaseCallIpAddr(const::std::string &, ::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SetZLIMSIpAddr(const::std::string &, ::Ice::Int, const::Ice::Current &cur)
{
}

::BGI::RPC::IpAddr ImagerControlMgrImp::GetZLIMSAddr(const::Ice::Current &cur)
{
	return ::BGI::RPC::IpAddr();
}

::BGI::RPC::SequenceTypes ImagerControlMgrImp::GetSequenceTypes(const::std::string &, const::Ice::Current &cur)
{
	return ::BGI::RPC::SequenceTypes();
}

bool ImagerControlMgrImp::SetClearData(const::BGI::RPC::DataList &, const::Ice::Current &cur)
{
	return false;
}

void ImagerControlMgrImp::StartRunLength(::Ice::Int, ::Ice::Int, ::Ice::Int, const::Ice::Current &cur)
{
}

void ImagerControlMgrImp::SetupRunInfo(const::BGI::RPC::RunInfo &, const::Ice::Current &cur)
{
}

bool ImagerControlMgrImp::ImagerSetup(const::BGI::RPC::ImagerSetupInfo & info, const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->ImagerSetup(info);
	}
	return false;
}

bool ImagerControlMgrImp::GoHome(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->GoHome();
	}
	return false;
}

bool ImagerControlMgrImp::GoLoadingPos(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->GoLoadingPos();
	}
	return false;
}

bool ImagerControlMgrImp::GoWashPos(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->GoWashPos();
	}
	return false;
}

bool ImagerControlMgrImp::IsAtLoadPosition(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->IsAtLoadPosition();
	}
	return false;
}

bool ImagerControlMgrImp::SwitchVaccum(bool value, const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->SwitchVaccum(value);
	}
	return false;
}

bool ImagerControlMgrImp::GetVaccumStatus(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->GetVaccumStatus();
	}
	return false;
}

bool ImagerControlMgrImp::PreInjectSRE(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->PreInjectSRE();
	}
	return false;
}

bool ImagerControlMgrImp::ImagerConfigSet(const::BGI::RPC::ImagerConfigData & config, const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->ImagerConfigSet(config);
	}
	return false;
}

::BGI::RPC::ImagerConfigData ImagerControlMgrImp::ImagerConfigGet(const::Ice::Current &cur)
{
	if (m_stage != nullptr) {
		return m_stage->ImagerConfigGet();
	}
	return ::BGI::RPC::ImagerConfigData();
}
