#pragma once

#include "ImagerControlMgrIPC.h"
#include "StageInterface.h"

class ImagerControlMgrImp : public BGI::RPC::ImagerControlMgrIPC
{
public:
	ImagerControlMgrImp(StageControl* stage);
	~ImagerControlMgrImp();

	// Inherited via ImagerControlMgrIPC
	virtual ::Ice::Int GetHeartbeat(const::Ice::Current & = ::Ice::Current()) override;
	virtual void StartSetup(const::Ice::Current & = ::Ice::Current()) override;
	virtual void SaveSelectedScriptFileName(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void ScanFlowcellBarcode(const::Ice::Current & = ::Ice::Current()) override;
	virtual void ScanReagentKitBarcode(const::Ice::Current & = ::Ice::Current()) override;
	virtual void OverrideFlowcellBarcode(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendReagentKit(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void StartRun(const::Ice::Current & = ::Ice::Current()) override;
	virtual void PauseScript(const::Ice::Current & = ::Ice::Current()) override;
	virtual void ResumeScript(const::Ice::Current & = ::Ice::Current()) override;
	virtual void StopScript(const::Ice::Current & = ::Ice::Current()) override;
	virtual void AbortScript(const::Ice::Current & = ::Ice::Current()) override;
	virtual ::Ice::Int RunScriptNow(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::ScriptRunInfo GetScriptRunInfo(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::MessageArray GetScriptMessages(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetupScriptFile(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::QCDataValues GetQCValues(const::std::string &, ::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendSampleId(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::LoginedInfo Authenicate(const::std::string &, const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::std::string GetLanguage(const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetLanguage(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::ExperimentInfo GetExperimentInfo(const::Ice::Current & = ::Ice::Current()) override;
	virtual void DownloadScriptFile(::std::string &, ::std::string &, ::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void UIOperation(const::std::string &, const::std::string &, const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void StartSelfCheck(::BGI::RPC::DeviceCheckTypeEnum, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::AlarmMessageList GetAlarmMessages(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::LogMessageList GetLogMessages(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void ClearAlarmMessages(const::BGI::RPC::ClearAlarmMessageIds &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void ReagentNeedle(bool, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SendExperimentType(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetBuzzerVolume(::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetBaseCallIpAddr(const::std::string &, ::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetZLIMSIpAddr(const::std::string &, ::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::IpAddr GetZLIMSAddr(const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::SequenceTypes GetSequenceTypes(const::std::string &, const::Ice::Current & = ::Ice::Current()) override;
	virtual bool SetClearData(const::BGI::RPC::DataList &, const::Ice::Current & = ::Ice::Current()) override;
	virtual void StartRunLength(::Ice::Int, ::Ice::Int, ::Ice::Int, const::Ice::Current & = ::Ice::Current()) override;
	virtual void SetupRunInfo(const::BGI::RPC::RunInfo &, const::Ice::Current & = ::Ice::Current()) override;

	// Need to implement
	virtual bool ImagerSetup(const::BGI::RPC::ImagerSetupInfo &, const::Ice::Current & = ::Ice::Current()) override;
	virtual bool GoHome(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool GoLoadingPos(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool GoWashPos(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool IsAtLoadPosition(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool SwitchVaccum(bool, const::Ice::Current & = ::Ice::Current()) override;
	virtual bool GetVaccumStatus(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool PreInjectSRE(const::Ice::Current & = ::Ice::Current()) override;
	virtual bool ImagerConfigSet(const::BGI::RPC::ImagerConfigData &, const::Ice::Current & = ::Ice::Current()) override;
	virtual ::BGI::RPC::ImagerConfigData ImagerConfigGet(const::Ice::Current & = ::Ice::Current()) override;

private:
	StageControl* m_stage = nullptr;
};
