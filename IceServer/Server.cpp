#include "Server.h"
#include "cpplogger.h"
#include "ImagerControlMgrImp.h"
#include "ImagerControlMgrCallbackServerImp.h"

using namespace std;

IceServer::IceServer(StageControl* stage) : m_stage(stage)
{
	CPP::CppLogger::Initialize(CPP::CppLogger::LOG_ALL_OUT, 31, ".\\logs\\server");
	CPP::CppLogger::Instance()->EnableDetail(false);
	CPP_LOG_INFO("Server Start");
}

void IceServer::Close()
{
	shutdown();
	waitForShutdown();
	CPP_LOG_INFO("Server Close");
}

bool IceServer::start(int, char*[], int& status)
{
	try
	{
		//加载配置文件
		auto props = communicator()->getProperties();
		props->load("settings.config");

		// 1.创建Adapter对象
		auto adapter = communicator()->createObjectAdapterWithEndpoints(props->getProperty("Identity"), props->getProperty("serverEndpoint"));
		auto adapterCB = communicator()->createObjectAdapterWithEndpoints(props->getProperty("IdentityCB"), props->getProperty("serverEndpointCB"));

		// 2.创建Printer对象
		auto ImagerSvr = new ImagerControlMgrImp(m_stage);
		m_cb = new ImagerControlMgrCallbackServerImp;

		// 3.添加Printer对象到Adapter对象
		adapter->add(ImagerSvr, communicator()->stringToIdentity(props->getProperty("Identity")));
		adapterCB->add(m_cb, communicator()->stringToIdentity(props->getProperty("IdentityCB")));

		// 4.激活当前的Adapter
		adapter->activate();
		adapterCB->activate();

		status = EXIT_SUCCESS;

		return true;
	}
	catch (const Ice::Exception& e) {
		CPP_LOG_ERROR(e.what());
	}
	catch (const std::string& msg) {
		CPP_LOG_ERROR(msg.c_str());
	}
	catch (const char* msg) {
		CPP_LOG_ERROR(msg);
	}
	catch (...)
	{
		CPP_LOG_ERROR("Unknown Error");
	}

	status = EXIT_FAILURE;

	return false;
}

void IceServer::handleInterrupt(int sig)
{
	switch (sig)
	{
	case CTRL_C_EVENT: //按下Ctrl+C
		break;
	case CTRL_CLOSE_EVENT:
	case CTRL_BREAK_EVENT:
	case CTRL_LOGOFF_EVENT:
	case CTRL_SHUTDOWN_EVENT:
		Close();
		Sleep(1000);
		break;
	default:
		break;
	}
}

void IceServer::SendImagerError(::BGI::RPC::ImagerErrorEnum error)
{
	if (m_cb != nullptr) {
		m_cb->SendImagerError(error);
	}
}

void IceServer::SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo & info)
{
	if (m_cb != nullptr) {
		m_cb->SendImagerRunInfo(info);
	}
}

void IceServer::SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo & info, ::BGI::RPC::ImagerErrorEnum error)
{
	if (m_cb != nullptr) {
		m_cb->SendImagerTaskEnd(info, error);
	}
}
