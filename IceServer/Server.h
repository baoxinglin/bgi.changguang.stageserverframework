#pragma once

#include <memory>
#include <Ice/Ice.h>
#include "StageInterface.h"
#include "ImagerControlMgrCallbackServerImp.h"

class IceServer : public Ice::Service, public MasterCallback
{
public:
	IceServer(StageControl* stage);
	void Close();

protected:
	virtual bool start(int, char*[], int& status);
	void handleInterrupt(int sig);

	// Inherited via MasterCallback
	virtual void SendImagerError(::BGI::RPC::ImagerErrorEnum error) override;
	virtual void SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo & info) override;
	virtual void SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo & info, ::BGI::RPC::ImagerErrorEnum error) override;

private:
	StageControl* m_stage = nullptr;
	ImagerControlMgrCallbackServerImp* m_cb = nullptr;
};
