#pragma once

#include "ImagerControlMgrIPC.h"

// Stage Get Pointer
class MasterCallback
{
public:
	virtual void SendImagerError(::BGI::RPC::ImagerErrorEnum error) = 0;
	virtual void SendImagerRunInfo(const::BGI::RPC::ImagerRunInfo & info) = 0;
	virtual void SendImagerTaskEnd(const::BGI::RPC::ImagerSetupInfo & info, ::BGI::RPC::ImagerErrorEnum error) = 0;
};

// Stage Implement
class StageControl
{
public:
	virtual bool ImagerSetup(const::BGI::RPC::ImagerSetupInfo & info) = 0;
	virtual bool GoHome() = 0;
	virtual bool GoLoadingPos() = 0;
	virtual bool GoWashPos() = 0;
	virtual bool IsAtLoadPosition() = 0;
	virtual bool SwitchVaccum(bool value) = 0;
	virtual bool GetVaccumStatus() = 0;
	virtual bool PreInjectSRE() = 0;
	virtual bool ImagerConfigSet(const::BGI::RPC::ImagerConfigData & config) = 0;
	virtual ::BGI::RPC::ImagerConfigData ImagerConfigGet() = 0;

	void SetCallback(MasterCallback* cb) { m_cb = cb; }

protected:
	MasterCallback* m_cb = nullptr;
};
