﻿#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <functional>

namespace CPP {

	class CppFileSystem
	{
	public:
		typedef std::vector<std::string> FilesList;
		typedef std::function<bool(const std::string& fileName)> CompareFunc;

	public:
		/// 判断文件夹是否存在
		static bool Exist(const std::string& fileName);

		/// 创建新文件夹
		static bool MakeDir(const std::string& dirPath);

		/// 删除文件夹
		static bool RemoveDir(const std::string& folderPath);

		/// 删除文件
		static bool RemoveFile(const std::string& fileName);

		/// 重命名文件
		static bool RenameFile(const std::string& oldFileName, const std::string& newFileName);

		/// 获取程序所在的路径
		static std::string GetWorkDir();

		/// 查找路径下的所有文件
		static bool FindFiles(FilesList& filesList, const std::string& folderPath, CompareFunc compare = nullptr, bool bFullPath = false, int depth = 0);

		virtual ~CppFileSystem();

	private:
		CppFileSystem();
	};

}
