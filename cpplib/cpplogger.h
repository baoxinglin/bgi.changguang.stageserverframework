﻿#pragma once

/// platform windows & linux
/// CppLogger Feture :
/// 1.Make Directory;
/// 2.Multi-Levels;
/// 3.Multi-Object;
/// 4.Format Output;
/// 5.Thread Safety;

#include <iostream>
#include <string>

/// 使用全局日志输出对象
#define CPP_LOGOUT(level, format, ...) CPP::CppLogger::Instance()->WriteLog(level, __LINE__, __FILE__, format, ##__VA_ARGS__)
#define CPP_LOG_INFO(format, ...) CPP_LOGOUT(CPP::CppLogger::LV_INFO, format, ##__VA_ARGS__)
#define CPP_LOG_WARNING(format, ...) CPP_LOGOUT(CPP::CppLogger::LV_WARNING, format, ##__VA_ARGS__)
#define CPP_LOG_ERROR(format, ...) CPP_LOGOUT(CPP::CppLogger::LV_ERROR, format, ##__VA_ARGS__)
#define CPP_LOG_FATAL(format, ...) CPP_LOGOUT(CPP::CppLogger::LV_FATAL, format, ##__VA_ARGS__)
#define CPP_LOG_DEBUG(format, ...) CPP_LOGOUT(CPP::CppLogger::LV_DEBUG, format, ##__VA_ARGS__)

/// 使用指定日志输出对象
#define CPP_X_LOGOUT(logger, level, format, ...) logger->WriteLog(level, __LINE__, __FILE__, format, ##__VA_ARGS__)
#define CPP_X_LOG_INFO(logger, format, ...) CPP_X_LOGOUT(logger, CPP::CppLogger::LV_INFO, format, ##__VA_ARGS__)
#define CPP_X_LOG_WARNING(logger, format, ...) CPP_X_LOGOUT(logger, CPP::CppLogger::LV_WARNING, format, ##__VA_ARGS__)
#define CPP_X_LOG_ERROR(logger, format, ...) CPP_X_LOGOUT(logger, CPP::CppLogger::LV_ERROR, format, ##__VA_ARGS__)
#define CPP_X_LOG_FATAL(logger, format, ...) CPP_X_LOGOUT(logger, CPP::CppLogger::LV_FATAL, format, ##__VA_ARGS__)
#define CPP_X_LOG_DEBUG(logger, format, ...) CPP_X_LOGOUT(logger, CPP::CppLogger::LV_DEBUG, format, ##__VA_ARGS__)

namespace CPP {

class CppLoggerPrivate;

class CppLogger
{
public:
    /// 输出模式
    enum LOG_MODE
    {
        LOG_STD_OUT     = 0x01,         /// 用于控制台程序在控制台打印日志
        LOG_FILE_OUT    = 0x02,         /// 用于GUI程序输出日志到指定文件
        LOG_ALL_OUT     = 0x03          /// 同时输出到控制台和指定文件
    };

    /// 日志级别
    enum LOG_LEVEL
    {
        LV_INFO         = 0x01,         /// 记录，不属于错误，仅作记录用
        LV_WARNING      = 0x02,         /// 警告，虽然不影响正常工作，但需要记录下来，作为分析问题的依据。如：某条指令经重发后OK，将记录重发过程
        LV_ERROR        = 0x04,         /// 错误，一般性错误，需要软件给出错误提示或能自动进入故障处理流程。如：试剂抽取失败
        LV_FATAL        = 0x08,         /// 致命错误，需要机器做出相应的应急措施。如：发生的错误会对机器或使用人员造成伤害
        LV_DEBUG        = 0x10,          /// 调试信息，仅在DEBUG模式下有效，Release模式下会关闭DEBUG输出
		LV_MAX			= 0x20
    };

    /// 初始化日志环境，必须在使用日志前调用
    static void Initialize(int logMod = LOG_STD_OUT, int logFilter = LV_INFO|LV_WARNING|LV_ERROR|LV_FATAL, const std::string& logDir = "log");

    /// 卸载日志环境，必须在程序退出前调用，否则可能引起内存泄漏
    static void Uninitialize();

    /// 获取日志实例
    static CppLogger* Instance(const std::string& name = std::string());

    /// 获取最近一次日志输出的文件名称
    std::string GetFileName();

	/// 打印日志详情
	void EnableDetail(bool bOutputLogDetail);

    /// 格式化输出日志
    void WriteLog(LOG_LEVEL level, int lineno, const char* filepath, const char* format, ...);

    virtual ~CppLogger();

private:
    CppLogger();
    CppLogger(const std::string& name);
    CppLoggerPrivate* _p = nullptr;
};

}
