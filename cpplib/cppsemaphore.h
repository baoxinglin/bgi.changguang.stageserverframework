﻿#pragma once

#include <mutex>
#include <condition_variable>

namespace CPP {

class SemaphorePrivate;

class Semaphore
{
public:
    Semaphore(unsigned int count = 0);
    ~Semaphore();

    void Clear();
    void Signal(unsigned int sigs = 1);
	bool Wait(int timeout = 0);
	bool TryWait();

private:
	SemaphorePrivate* _p = nullptr;
};

}
